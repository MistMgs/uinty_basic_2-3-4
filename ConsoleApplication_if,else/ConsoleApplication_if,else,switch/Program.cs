﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_if_else_switch
{
    public enum Country { Korea,China,Japan};
    class Program
    {
        static void Main(string[] args)
        {
            Country myCountry = Country.Korea;
            if(myCountry== Country.Korea)
            {
                Console.WriteLine("한국");
            }
            else if (myCountry==Country.Japan)
            {
                Console.WriteLine("일본");

            }
            else if (myCountry== Country.China)
            {
                Console.WriteLine("중국");
            }
            else // Country에 속하는 항목이 없을경우
            {
                Console.WriteLine("선택된 나라가 없습니다.");
            }
        }
    }
}
