﻿using System;
using System.Collections.Generic; //using 블럭 선언
using System.Linq;
using System.Text;
using System.Threading.Tasks; 

namespace ConsoleApplication1  //나만의 네임 스페이스 선언
{
    class Program //클래스 선언
    {
        //XML 주석
        static void Main(string[] args)  //메인함수
        {
            //단일 라인 주석
            /* 
             
             */
            Console.WriteLine("Hellow World !");
        }
    }
}
