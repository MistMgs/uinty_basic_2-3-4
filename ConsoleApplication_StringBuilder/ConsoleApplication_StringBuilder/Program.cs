﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; //StringBuilder
using System.Threading.Tasks;
using System.Diagnostics;//Stopwatch


//StringBuilder 


namespace ConsoleApplication_StringBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            string text1 = "";
            Stopwatch sw1 = Stopwatch.StartNew();
            for (int i = 0; i < 50000; i++)
            {
                text1 += i.ToString();

            }

            sw1.Stop();


            StringBuilder text2 = new StringBuilder();

            Stopwatch sw2 = Stopwatch.StartNew();
            for(int i =0;i<500000; i++)
            {
                text2.Append(i.ToString());
            }
            sw2.Stop();

            Console.WriteLine("string:{0}  Milliseconds", sw1.ElapsedMilliseconds);
            Console.WriteLine("stringBuilder:{0}  Milliseconds", sw2.ElapsedMilliseconds);
        }

    }
}
