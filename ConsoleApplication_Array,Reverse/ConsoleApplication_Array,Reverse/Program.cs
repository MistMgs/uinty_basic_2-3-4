﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//배열 정렬
namespace ConsoleApplication_Array_Reverse
{

    class Program
    {
        static void Main(string[] args)
        {
            int[] array1 = new int[] { 3, 7, 6, 2, 8, 9, 5, 1, 4 };

            Console.WriteLine("==정렬 전==");
            foreach(var item in array1)
            {
                Console.WriteLine("{0}", item);

            }

            Console.WriteLine("\r\n==Array.Sort==");
            Array.Sort(array1);
            foreach(var item in array1)
            {
                Console.WriteLine("{0}", item);
            }

            Console.WriteLine("\r\n==Array.Reverse==");
            Array.Reverse(array1);
            foreach (var item in array1)
            {
                Console.WriteLine("{0}",item);
            }
            Console.WriteLine();
        }
    }
}
