﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4
{
    class Nullable
    {
        static void Main(string[] args)
        {
            //int에 null 가능 하도록 선언
            int? number = null;
            number = 10;
            if(number.HasValue)
            {
          
                Console.WriteLine(number.Value); 
            }
            else
            {
                Console.WriteLine("값이 null입니다.");
            }
        }
   
    }
}
