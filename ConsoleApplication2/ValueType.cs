﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class ValueType
    {
        static void Main(string[] args)
        {
            int number1 = 123;
            System.Int32 number2 = 123;
            Console.WriteLine("number1:{0}", number1);
            Console.WriteLine("number2:{0}", number2);

            double number3 = 123D;
            double number4 = 123;
            Console.WriteLine("number3:{0}", number3);
            Console.WriteLine("number4:{0}", number4);

        }
    }
}
