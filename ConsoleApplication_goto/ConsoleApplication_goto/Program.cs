﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_goto
{
    class Program
    {
        static void Main(string[] args)
        {
            int result = 0; 
            for(int i=1; i<=10;i++)
            {
                result += i;

                if(i==5)
                {
                    goto Jump; //점프 라벨로 이동
                }
            }
            Console.WriteLine("for문 1~10더하기:{0}", result);
        Jump: //goto 문이 이동할 라벨 지정
            Console.WriteLine("goto점프!:{0}", result);
        }
    }
}
