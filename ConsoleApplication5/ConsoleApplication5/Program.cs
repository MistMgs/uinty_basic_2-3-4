﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Program
    {
        static void Main(string[] args)
        {
            int? number = null;
            //?연산자 사용
            int defaultNumber1 = (number == null) ? 0 : (int)number;
            Console.WriteLine("defaultNumber1:{0}", defaultNumber1);
            //??연산자 사용 number가 null 일 경우 0을 기본값으로 설정하여 defaultNumber에 설정
            int defaultNumber2 = number ?? 0;
            Console.WriteLine("defaultNumber2:{0}", defaultNumber2);
            }
    }
}
