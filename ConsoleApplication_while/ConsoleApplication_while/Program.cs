﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_while
{
    class Program
    {
        static void Main(string[] args)
        {
            int result = 0;
            int startNumber = 1;
            int maxValue = 10;
            while (startNumber<=maxValue)
            {
                result += startNumber;
                startNumber++; //startNumber=startNumber+1; 축약표현

            }
            Console.WriteLine("while문1~10더하기:{0}", result);
        }
    }
}
