﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//[천 단위 컴마(,) 지정] 

namespace ConsoleApplication_comma
{
    class Program
    {
        static void Main(string[] args)
        {
            int number1 = 12345;
            int number2 = -12345;

            Console.WriteLine("number1:{0:#,#;(#,#)}", number1);  //number1 : 12,345
            Console.WriteLine("number2:{0}", number2.ToString("#,#;(#,#)"));  //number1 : (12,345) 왜 음수가 양수로 변하는가???????????????
        }
    }
}
