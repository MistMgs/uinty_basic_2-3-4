﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    public enum Country { Korea,China,Japan}
    class Program
    {
        static void Main(string[] args)
        {
            Country location1 = Country.Korea;
            Country location2 = (Country)1;
            int location3 = (int)location1;

            Console.WriteLine("location1 : {0}", location1);
            Console.WriteLine("location2 : {0}", location2);
            Console.WriteLine("location3(int) : {0}", location3);
            Console.WriteLine("location3(Country) : {0}", (Country)location3);

        }
    }
}
