﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_Parse
{
    class Program
    {

        static void Main(string[] args)
        {
           
                string numberString ="123";
                //Convert 클래스 이용

                int number1 =Convert.ToInt32(numberString);
                //Parse 메소드 이용??????????
                //int number2 =Int32.Parse(numberString);

           int number2 = int.Parse(numberString);

                Console.WriteLine("number1:{0}",number1);
                Console.WriteLine("number2:{0}",number2);
           }
        }
    }
