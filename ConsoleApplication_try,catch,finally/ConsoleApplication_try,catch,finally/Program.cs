﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_try_catch_finally
{
    class Program
    {
        static void Main(string[] args)
        {
            Foo("123");     //올바른 처리
            Foo(null);      //ArgumentNullException 발생
            Foo("일이삼");  //Exception 발생
        }
        static void Foo(string data)
        {
            //복수개의 catch 문이 올 수도 있다.
            try
            {
                int number = Int32.Parse(data);
                Console.WriteLine("number:{0}", number);
            }
            catch(ArgumentException ex) //입력 인자가 없을 경우
            {
                Console.WriteLine("ArgumentNullExxeption처리:{0}", ex.Message);
            }
            catch(Exception ex) //최상위 예외 형식
            {
                Console.WriteLine("Exception처리:{0}", ex.Message);
            }
        }
    }
}
