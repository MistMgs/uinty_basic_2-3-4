﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_throw
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ShowMessage("메시지를 출력합니다");
                ShowMessage(null);
            }
            catch(ArgumentNullException ex)
            {
                //예외처리
                Console.WriteLine("ArgumentNullException처리:{0}", ex.Message);
            }
        }
        //throw 구문작성
        static void ShowMessage(string message)
        {
            if(message== null)
            throw new ArgumentNullException("message");

            Console.WriteLine(message);
        }


    }
}
