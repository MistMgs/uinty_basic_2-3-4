﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_TwoRank
{
    class Program
    {
        static void Main(string[] args)
        {
            string[,] array2D = new string[3, 5];

            Console.WriteLine("2차원 배열");
            //배열 값 설정
            for (int i=0; i<array2D.GetLength(0);i++) //arrray2.GetLength(0):3
            {
                for (int j =0; j<array2D.GetLength(1);j++) //arrray.GetLength(1):5
                {
                    array2D[i,j]=String.Format("{0}-{1}",i,j);
                }
            }

            //for 문 사용 

            for(int i=0; i<array2D.GetLength(0);i++)
            {
                for (int j=0; j<array2D.GetLength(1);j++)  //arrray2.GetLength(0):3
                {
                    Console.WriteLine("{0})",array2D[i,j]);//arrray.GetLength(1):5
                }
                Console.WriteLine();
            }
        }

    }
}
