﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_switch
{
    public enum Country { Korea, China, Japen };

    class Program
    {
        static void Main(string[] args)
        {

            Country myCounrty = Country.Korea;
            switch (myCounrty)
            {
                case Country.Korea:
                    Console.WriteLine("한국");
                    break;
                case Country.China:
                    Console.WriteLine("중국");
                    break;
                case Country.Japen:
                    Console.WriteLine("일본");
                    break;
                default: //Counrty형식에 속하는 항목이 없을경우
                    Console.WriteLine("선택된 나라가 없습니다.");
                    break;
            }

        }
    }
}
