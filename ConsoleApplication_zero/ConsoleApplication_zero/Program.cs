﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//[특정 길이까지 남는 앞 부분을 0으로 채우기] 
namespace ConsoleApplication_zero
{
    class Program
    {
        static void Main(string[] args)
        {
            int number1 = 1;
            int number2 = 123;
            int number3 = 1234;


            Console.WriteLine("number1:{0:000##}", number1);    // number1 : 00001
            Console.WriteLine("number2:{0:0000#}", number2);// number2 : 00123
            Console.WriteLine("number3:{0}", number3.ToString("00000")); // number3: 01234
        }
    }
}
