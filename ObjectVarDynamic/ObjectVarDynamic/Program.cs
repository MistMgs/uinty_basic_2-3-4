﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectVarDynamic
{
    class Program
    {
        static void Main(string[] args)
        {
            string valuel = "값1";
            var value2 = "값2";
            dynamic value3 = "값3";

            Console.WriteLine("Value1:{0}", valuel);
            Console.WriteLine("Value2:{0}", value2);
            Console.WriteLine("Value3:{0}", value3);
        }
    }
}
