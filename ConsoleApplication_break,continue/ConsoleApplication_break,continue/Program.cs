﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_break_continue
{
    class Program
    {
        static void Main(string[] args)
        {
            int result = 0;

            for(int i=1; i <=100; i++)
            {
                if(i>10) //i가10보다 크면 for문을 빠져 나감.
                {
                    break;
                }
                if((i%2)==0) //짝수는 건너뛴다.
                {
                    continue;
                }
                result += i;
            }
            Console.WriteLine("break-continue 1~10 중 홀수만 더하기:{0}", result);
        }
    }
}
