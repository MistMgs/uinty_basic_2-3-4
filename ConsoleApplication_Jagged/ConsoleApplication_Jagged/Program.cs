﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_Jagged
{
    class Program
    {
        static void Main(string[] args)
        {
            string[][] arrayJagged = new string[3][];
            arrayJagged[0] = new string[5]; //각 1차원 배열 별로 크기가 다르게 지정.
            arrayJagged[1] = new string[2];
            arrayJagged[2] = new string[3];



            Console.WriteLine("Jagged배열");


            //배열 값 설정
            for (int i = 0; i < arrayJagged.GetLength(0); i++)
            {
                for (int j=0; j<arrayJagged[i].Length;j++)
                {
                    arrayJagged[i][j] = String.Format("{0}-{1}", i, j);
                }
            }



            //for문 사용

            for (int i = 0; i < arrayJagged.GetLength(0); i++)
            {
                for (int j = 0; j < arrayJagged[i].Length; j++)
                {
                    Console.WriteLine("({0})", arrayJagged[i][j]);

                }
                Console.WriteLine();
            }
        }
    }
}
