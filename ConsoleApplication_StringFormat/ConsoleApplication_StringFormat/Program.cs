﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//문자열 포맷(Console.WriteLine, String.Format) 

namespace ConsoleApplication_StringFormat
{
    class Program
    {
        static void Main(string[] args)
        {
            int number1 = 1;
            int number2 = 123;

            Console.WriteLine("number1:|{0,5}|,number2:{1:0000#}"
                , number1, number2); // number1:|1|,number2:00123
           Console.WriteLine("number1:|{0,-5}|,number2:{1:0000#}"
                , number1, number2); // number1:|1|,number2:00123

           int number3 = 12345;
           Console.WriteLine("number3:{{{0}}}", number3);//number3:{12345}

        }
    }
}
